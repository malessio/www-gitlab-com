require 'generators/direction'
require 'generators/releases'
require 'generators/org_chart'
require 'extensions/breadcrumbs'
require 'lib/homepage'

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  ugly: true,
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :blog do |blog|
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/blog/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  activate :minify_css
  activate :minify_javascript
  activate :minify_html

  # Populate the direction and releases pages only on master.
  # That will help shave off some time of the build times on branches.
  if ENV['CI_BUILD_REF_NAME'] == 'master' || !ENV.key('CI_BUILD_REF_NAME')
    ## Direction page
    if PRIVATE_TOKEN
      proxy '/direction/index.html', '/direction/template.html', locals: { direction: generate_direction, wishlist: generate_wishlist }, ignore: true
      proxy '/direction/product-vision/index.html', '/direction/product-vision/template.html', locals: { product_vision: generate_product_vision }, ignore: true
    end

    ## Releases page
    releases = ReleaseList.new
    proxy '/releases/index.html', '/releases/template.html', locals: { list: releases.generate }, ignore: true
  end
end

org_chart = OrgChart.new
proxy '/team/chart/index.html', '/team/chart/template.html', locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy comparison PDF pages
data.features.comparisons.each do |key, comparison|
  file_name = key.dup.tr('_', '-')
  proxy "/comparison/pdfs/#{file_name}.html", '/comparison/pdfs/template.html', locals: {
    comparison_block: comparison,
    key_one: comparison.product_one.to_sym,
    key_two: comparison.product_two.to_sym
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Proxy case study pages
if @app.data.respond_to?(:case_studies)
  data.case_studies.each do |filename, study|
    proxy "/customers/#{filename}/index.html", '/templates/case_study.html', locals: { case_study: study }
  end
end

page '/404.html', directory_index: false

ignore '/templates/*'
ignore '/direction/template.html'
ignore '/direction/product-vision/template.html'
ignore '/includes/*'
ignore '/releases/template.html'
ignore '/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
