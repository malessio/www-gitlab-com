---
layout: markdown_page
title: "GitLab 101"
---

## GitLab 101s

### CEO 101

There is a monthly GitLab CEO 101 call with new hires and the CEO. Here we talk about the following topics. This is a zoom call. Please read the history and values links provided below and come prepared with questions. The call will be driven by questions asked by new employees. Make sure your full name is set in Zoom.

1. Questions about GitLab history [https://about.gitlab.com/history/](https://about.gitlab.com/history/)
1. Questions about our values [https://about.gitlab.com/handbook/values](https://about.gitlab.com/handbook/values)
1. Team structure [https://about.gitlab.com/team/structure/](https://about.gitlab.com/team/structure/) or [organization chart](/team/chart)
1. How we work [https://about.gitlab.com/handbook/general-guidelines](https://about.gitlab.com/handbook/general-guidelines)
  - Please pay special attention to the guideline about developing content and procedures in a public, transparent, [handbook-first](/handbook/general-guidelines/#handbook-first) manner

All questions and answers during the call are added to the FAQ on [https://about.gitlab.com/culture/gitlab-101/](https://about.gitlab.com/culture/gitlab-101/).
This is done during the call to immediately make any changes; for example we created [this commit](https://gitlab.com/gitlab-com/www-gitlab-com/commit/8cf1b0117dce5439f61e207315f75db96c917056) together on the call.

The calendar invite will say: "You need to prepare for this call, please see https://about.gitlab.com/culture/gitlab-101/ for details."

## People Ops 101
There is a monthly People Ops 101 Zoom call with new hires, our Chief Culture Officer (CCO), and members of the PeopleOps. It is meant to occur following GitLabbers' participation in the CEO 101. The links above are good reading for both meetings. In addition to the topics above, new hires may be interested in our culture, [people priorities](https://about.gitlab.com/okrs/), [diversity](https://about.gitlab.com/handbook/values/#diversity) and [inclusion](https://about.gitlab.com/culture/inclusion/), [hiring](https://about.gitlab.com/handbook/hiring/) and a variety of other topics. Our CCO welcomes the team's questions and comments, but also appreciates hearing suggestions and ideas from GitLab's new hires.

### New Teammates Introduction

Held immediately before the CEO 101, everyone introduces themselves using the order in the calendar invite (please skip the CEO):

  - What do you do at GitLab?
  - Why did you join GitLab?
  - What do you enjoy in your private life (quirky details are encouraged, also feel free to introduce your significant other and/or pets)?
  - Did you already have 10 virtual coffee breaks?
  - If you already had 10 are you open to more?
  - Invite the next person to talk.

### People Ops 101s

There is a monthly People Ops 101 Zoom call with new hires, our Chief Culture Officer (CCO), and members of the People Ops team. It is meant to occur following GitLabbers' participation in the CEO 101. The links above are good reading for both meetings. In addition to the topics above, new hires may be interested in our culture, [people priorities](https://about.gitlab.com/okrs/), [diversity](https://about.gitlab.com/handbook/values/#diversity) and [inclusion](https://about.gitlab.com/culture/inclusion/), [hiring](https://about.gitlab.com/handbook/hiring/), and a variety of other topics. Our CCO welcomes the team's questions and comments, but also appreciates hearing suggestions and ideas from GitLab's new hires.

### Frequently Asked Questions about the GitLab Culture

##### Q: understand that one of the two largest telecommunications operators in the United States wanted to copy our handbook?
We feel that there are definitely some parts that companies can take home from our handbook, but a handbook should be specific on what you do, not on what you want to be. Every company will be able to fill out their own handbook over time.

##### Q: What could slow us down with making sure the enterprise edition is successful?
Losing the interest of the open source community. If someone wants to make a contribution for CE and decides not to merge it because it’s an EE feature for example. Saas will be very important; it will be the largest part of revenue in 2019
A great example of this is Netflix. Everyone knew that video on demand was the future. Netflix however started shipping dvds over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.
GitHub is longer around, but we feel it’s still early. Tools don’t collaborate and we can try to do a better job than our competition.

##### Q: If an EE customer wants to pay for us to develop a specific feature, how do we manage this?
It’s great someone wants to pay to have a feature built, it means they really need it. However, we will not make a custom version of GitLab, even gitlab.com is running on EE, we move faster that way. Working this was minimizes technical complexity to determine features to follow after, it’s a trade off to make. This doesn’t mean that no is always going to stay no. Dmitriy was against open projects. We keep an open mind for improvements.

##### Q: What do you think are the things that could slow us down in the future that we should be aware of as a team and product?
A few things have come up, and we feel the best example is proposing 2 month release cycle. The argument was that we have a lot of bug fixes every time and it would give us more time to make a feature. We don’t feel that is the case. We aim to make the absolute smallest thing possible other wise it will slow you down. We would love to work on a 2 week release cycle but that should be another conversation.

##### Q: Do we split up cost on how we invest our cash?
It’s hard to be open about financials; it’s easy when everything is going well but very hard when it’s not. We try to be open to our team as we go along but it will hurt being open to the outside world about this in the future.

##### Q: Why do we work on Gogs, since it looks kind of similar to what we do at GitLab?
Gogs has a similarity with the core function of GitLab. Many times with competitors, we feel we can do the same, we can make that feature, we can add it to GitLab. What Gogs does is similar to GitLab but with less CPU and memory which is very difficult for us. Since it can host repositories for personal projects of a few people we feel it’s complementary to GitLab since we don’t service that target audience. There is still a place in the market for this tool and it’s genuinely useful so let’s make sure it’s a great tool for people to use. If you’re using GitLab and want to downgrade or are using Gogs and need more, the integration is simple and easy.

##### Q: When do you feel we should do an IPO?
Basically you can do an IPO when you have $100 million in Annual Recurring Revenue (ARR). After we cross that threshold we can start thinking about IPO, before that we’re too small.

##### Q: Do you feel 2020 is very aggressive timing for an IPO?
It's aggressive, it's possible, and it's realistic. Having a goal gives us clarity on what we need to achieve. Our ambition is clear, and we want to be a growing and independent company. We are in an enormous market, and we're winning that market.

##### Q: What do you feel are the challenges in becoming the number one solution that companies standardize on?
If we can make a product that is strong with all features from planning to monitoring, and it works well, then you have a chance. It should have benefits that you can only have with an integrated product, such as Cycle Analytics.

##### Q: Is it worth investing in trying to win the open source market from our competitors?
Our plan is to start with becoming the most popular tool for people’s own git hosting service; we’ve managed that so far. Secondly we want to get to be the one with the most revenue. Thirdly we want to become the most popular tool for hosting private repos. Once we’ve reached that we want to be the most popular tool for hosting public repos and lastly we want to be the number one tool for people to host not code but books, tech papers, visual models. movies etc. More info on this is on our [strategy page](https://about.gitlab.com/strategy/)

##### Q: Doesn't constant iteration sometimes lead to shipping lower quality work and a worse product?
We know that iteration is painful. If you're doing iteration correctly, it should be painful. However, if we take smaller steps and ship smaller simpler features, we get feedback sooner. Instead of spending time working on the wrong feature or going in the wrong direction, we can ship the smallest product, receive fast feedback, and course correct.

##### Q: When did you know that GitLab would work and that it would attract a community?
It was iterative. The first time was when Sid saw GitLab and how easy it was to collaborate. The second time was when he first made a post about it on Hacker News, and it didn't trend at first, so he left his computer to go make pancakes. But he had his phone with him, and his post started to get tons of comments and was featured on the home page. He asked his wife to take over making the pancakes for a few minutes, but he never came back because he spent the rest of the day answering questions about GitLab. Another time was when GitLab got into Y Combinator.

##### Q: What were the biggest challenges you faced as a team as a result of Y Combinator?
We became much more comfortable with a much faster pace. This changed the way we thought about what we can achieve in a short timeframe. If we think something takes too long, we need to change our idea of what we can accomplish. There is always an opportunity to do something smaller and imperfect but that still makes a difference.

##### Q: How do you feel the Y Combinator experience helped GitLab?
It was essential to the trajectory we're at. We never thought we could beat our competitors, but we had big ambitions and knew we had a great product and would continue to iterate and improve on that product, which we still do today. We keep in touch with a lot of the people we met at Y Combinator, and we help other start-ups with their applications and interview training, and we spread the word that it's a great place to join and develop your company.

##### Q: Where do you draw your inspiration for our values?
We take inspiration from other companies, and we always go for the boring solutions. Just like the rest of our work, we continually adjust our values and try to always make them better. We used to have more values, but it was difficult to remember them all, so we condensed them and gave sub-values and created an acronym. Everyone is welcome to suggest on how to improve them.

##### Q: Do you envision transparency being a core value for GitLab forever, even after IPO?
Yes. Often company cultures get diluted as they grow, most likely because they do not write anything down. But we will make sure our culture scales with the company. When we go public, we can declare everyone in the company as an insider, which will allow us to remain transparent internally about our numbers, etc. Everything else that can be transparent will continue to be so.

##### Q: How did the handbook start?
The handbook started when there were only 10 people, to make sharing information efficient and easy. People won't see emails about process changes that were sent before the joined, and most of the people who will eventually join GitLab have not joined yet, so we need to have the information accessible to everyone.

##### Q: I read that GitLab is not a democracy but everyone is encouraged to contribute, ask questions, and challenge. How do you view that hierarchy, and how are decisions made from the top-down?

There's a trade-off between speed and breadth when making decisions. A lot of companies try to find the ideal trade-off between speed and breadth. But you can keep trying this since every time you move towards one side you move away from the other. At GitLab we solve this trade-off by distinguishing between two phases in the decision making process, the data gathering phase and the conclusion phase. During the data gathering phase we allow everyone to give their data and point of view. We are on the extreme side since we not only ask people in the hierarchy to contribute, as well as the rest of the company, but we also open it up to people outside of the company to contribute as well. In other companies a wide breadth of opinions causes a problem during the conclusion phase. This because there are many opinions and it takes a long time to reach consensus. At GitLab, we give authority to the person who has to do the work and their chain of command. They are experts and they make the decisions and will have to live with them. If they make a mistake, at least it's something they chose and will be motivated to fix. This way we have the best of both worlds, we get a huge breadth while also having a speedy process. Everyone can contribute, but at the same time we want results and encourage ownership.

##### Q: Why are some team members hired as contractors and not as employees?

We do not have an entity in each country. We are expanding entities slowly and based among other factors on how many people are contractors in the country.

##### Q: What are the plans for GitLab.com in general for SaaS?

By the end of the year, GitLab.com should be ready for mission critical workloads. We are currently making changes in the company and working with developers/infrastructure to ensure it becomes a viable competitor to other SaaS services.

##### Q: How much fear is there in our user base that the open source edition of GitLab will deteriorate?

We have tried different business models and many didn’t work. As a company, we realized we need recurring revenue. To obtain that, we introduced Enterprise Edition. Initially, everyone was worried innovation would stop on the Community Edition. Within six months the Enterprise Edition was under the MIT license. The community saw we were building both products, which helped to build trust. Customers became confused, so after six months the arrangement stopped. Response was positive about the shift. We should keep improving both and keep our [stewardship promises](https://about.gitlab.com/stewardship).

##### Q: How do you determine the line between open source and proprietary features?
It's ambiguous and difficult to get right. We listened to the community to find a good balance, and we were willing to iterate and make changes in order to make the community happy. The value needs to be there in order for people to pay for premium, and we think we provide that.

##### Q: Is there a plan for if a new version control technology comes out?
We will adopt it and keep an open mind, and hopefully we will be big enough at that point that people will consider us an integrated DevOps product. If not, we can always change our name, but we are investing to make git better for everyone.
