---
layout: markdown_page
title: "GitLab Direction"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page describes the direction and roadmap for GitLab. It's organized from
the short to the long term.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting Merge Requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+Merge+Requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## What our customers want

As a company, GitLab tries to make things that are useful for our customers as
well as ourselves. After all, GitLab is one of the biggest users of GitLab. If a
customer requests a feature, it carries extra weight. Due to our short release
cycle, we can ship simple feature requests, such as an API extension, within one
or two months.

## Previous releases

On our [releases page](/releases/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Future releases

GitLab releases a new version [every single month on the 22nd]. Note that we
often move things around, do things that are not listed, and cancel things that
_are_ listed.

This page is always in draft, meaning some of the things here might not ever be
in GitLab. New premium features are indicated with "Premium" label. This is
our best estimate of what will be new premium features, but is in no way
definitive.

The list is an outline of **tentpole features** -- the most important features
of upcoming releases -- and doesn't include any contributions from volunteers
outside the company. This is not an authoritative list of upcoming releases - it
only reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).

<%= direction %>

## Enterprise Editions

### Starter

Starter features are available to anyone with an Enterprise Edition subscription (Starter, Premium, Ultimate).

<%= wishlist["GitLab Starter"] %>

### Premium

Premium features will only be available to Premium (and Ultimate) subscribers.

<%= wishlist["GitLab Premium"] %>

### Ultimate

Ultimate is for organisations that have a need to build secure, compliant
software and that want to gain visibility of - and be able to influence - their
entire organisation from a high level.

From a high level, the first major initiatives for Ultimate are:

- [Portfolio Management](https://gitlab.com/groups/gitlab-org/-/epics/48)
- [Security scanning](https://gitlab.com/gitlab-org/gitlab-ee/issues/3723) and [Software Composition Analysis](https://gitlab.com/gitlab-org/gitlab-ee/issues/2592)

Ultimate features will only be available to Ultimate subscribers.

Individual features:

<%= wishlist["GitLab Ultimate"] %>

[every single month on the 22nd]: /2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab

## Product Vision

See [Product Vision](product-vision).

## Functional Areas

Below are features that represent the various functional areas we see GitLab going in. This list is not
prioritized. We invite everyone to join the discussion by clicking on the
items that are of interest to you. Feel free to comment, vote up or down any issue
or just follow the conversation. For GitLab sales, please add a link to the
account in Salesforce.com that has expressed interest in a wishlist feature. We
very much welcome contributions that implement any of these things.

### Build and packaging

GitLab is the engine that powers many companies' software businesses so it is important to ensure it is as easy as possible to deploy, maintain, and stay up to date.

Today we have a mature and easy to use Omnibus based build system, which is the foundation for nearly all methods of deploying GitLab. It includes everything a customer needs to run GitLab all in a single package, and is great for installing on virtual machines or real hardware. We are committed to making our package easier to work with, highly available, as well as offering automated deployments on cloud providers like AWS.

We also want GitLab to be the best cloud native development tool, and offering a great cloud native deployment is a key part of that. We are focused on offering a flexible and scalable container based deployment on Kubernetes, by using enterprise grade Helm Charts.

#### GitLab High Availability
<%= wishlist["HA"] %>

### CI / CD

We want to help developers get their code into production; providing convenience and confidence to the developer in an integrated way. CI/CD focuses on steps 6 through 9 of our [scope](#scope): Test (CI), part of Review (MR), Staging (CD), and part of Production (Chatops). When viewed through the CI/CD lens, we can group the scope into CI, CD, and things that are currently beyond any definition of CD.

![GitLab CI/CD Scope](/images/direction/cicd/revised-gitlab-ci-scope.svg)

We define our vision as “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)”: leveraging our [single application](https://about.gitlab.com/handbook/product/single-application/), it is simple to assist users in every phase of the development process, implementing automatic tasks that can be customized and refined to get the best fit for their needs.
Our idea is that the future will have “auto CI” to compile and test software based on best practices for the most common languages and frameworks, “auto review” with the help of automatic analysis tools like Code Climate, “auto deploy” based on Review Apps and incremental rollouts on Kubernetes clusters, and “auto metrics” to collect statistical data from all the previous steps in order to guarantee performances and optimization of the whole process.
Dependencies and artifacts will be first-class citizens in this world: everything must be fully reproducible at any given time, and fully connected as part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c).

Many of the issues describe development of an n-tier web app, but could equally be applied to an iOS app, Ruby gem, static website, or other type of project.

See a slightly more complete rendering of an [example pipeline](complex-pipeline.svg).
{: .note}

<!--{: #sample .alert .alert-info}-->

#### Pipelines

<%= wishlist["pipeline"] %>

#### Build

GitLab CI provides an explicit `build` stage and the concept of build artifacts, but we might need to separate out the build artifacts from test artifacts. For example, you might want your test runner to create a JUnit-style output file which is available for external consumption, but not included in the build image sent to production. Creation of an explicit build aligns well with Docker where the result of the build stage is a Docker image which is stored in a registry and later pulled for testing and deployment.

<%= wishlist["ci-build"] %>

#### Test

<%= wishlist["test"] %>

#### Deploy

A key part of CD is being able to deploy. We currently have this ability via scripts in the `deploy` stage in `.gitlab-ci.yml`. We will go further.

<%= wishlist["deploy"] %>

#### Monitor

See [Monitoring](#monitoring).
{: .note}

#### Misc

<%= wishlist["CI/CD"] %>

### Code Review

<%= wishlist["code review"] %>

### Container Registry

<%= wishlist["container registry"] %>

### Moderation Tools

<%= wishlist["moderation"] %>

### Open Source Projects

<%= wishlist["open source"] %>

### Pages

<%= wishlist["pages"] %>

### Performance

<%= wishlist["performance"] %>

### Monitoring

Performance is a critical aspect of the user experience, and ensuring your application is responsive and available is everyone's responsibility. We want to help address this need for development teams, by integrating key performance analytics and feedback into the tool developers already use every day.

As part of our commitment to performance we are also deeply instrumenting GitLab itself, enabling our team to improve GitLab peformance and for customers to more easily manage their deployments.

<%= wishlist["Monitoring"] %>

### Service Desk

<%= wishlist["service desk"] %>

### Usability

<%= wishlist["usability"] %>

### Version Control for Everything

<%= wishlist["vcs for everything"] %>

### Workflow management with issues

<%= wishlist["issues"] %>

### Discussion

#### Product debt and maintenance
We are fixing product debt and maintaining the core areas of Discussion, in order to improve the baseline experience and introduce functionality that users _already expect_.
- [UX: Merge Request Page Vision](https://gitlab.com/groups/gitlab-org/-/epics/42)
- [Merge request file nav UX improvements](https://gitlab.com/groups/gitlab-org/-/epics/11)
- [Milestones UX improvements](https://gitlab.com/groups/gitlab-org/-/epics/5)
- [Group milestones parity with project milestones](https://gitlab.com/groups/gitlab-org/-/epics/6)
- [Labels UX improvements](https://gitlab.com/groups/gitlab-org/-/epics/8)
- [Subgroup support for issues, merge requests, labels, and milestones](https://gitlab.com/groups/gitlab-org/-/epics/22)

#### Real-time experience
GitLab is a modern web app. Users expect UI elements to update in real-time automatically, and behave more app-like, instead of a static website.
- [Real-time updates](https://gitlab.com/groups/gitlab-org/-/epics/14)
- [Real-time editing of issues and merge requests](https://gitlab.com/groups/gitlab-org/-/epics/52)

#### Merge requests, approvals, and code review
We are focusing on developer-centric workflows and bringing many features _long-requested by developers_. GitLab started as a tool for developers to do their work. We are prioritizing our efforts to bring this new wave of developer-focused functionality to GitLab.
- [Multiple assignees for merge requests](https://gitlab.com/gitlab-org/gitlab-ee/issues/2004)
- [Group merge requests](https://gitlab.com/gitlab-org/gitlab-ee/issues/3427)
- [Increase approvals usage](https://gitlab.com/groups/gitlab-org/-/epics/2)
- [More granular approvals](https://gitlab.com/groups/gitlab-org/-/epics/3)
- [Approvals in merge request list](https://gitlab.com/groups/gitlab-org/-/epics/1)
- [Multiple blocking approver groups](https://gitlab.com/gitlab-org/gitlab-ee/issues/1979)
- [Approvers based on code owners](https://gitlab.com/gitlab-org/gitlab-ee/issues/1012)
- [Approvers based on code Git blame](https://gitlab.com/gitlab-org/gitlab-ee/issues/499)
- [Approvers based on code history](https://gitlab.com/gitlab-org/gitlab-ee/issues/470)
- [Batch comments on merge requests](https://gitlab.com/groups/gitlab-org/-/epics/23)
- [Viewing a large diff of a single file](https://gitlab.com/groups/gitlab-org/-/epics/15)
- [Commenting on merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/19)
- [Select author and customize commit message in squashed commit](https://gitlab.com/groups/gitlab-org/-/epics/9)
- [Subscribe to code changes](https://gitlab.com/gitlab-org/gitlab-ee/issues/1817)

#### Portfolio management and issue management
We are moving into the new area of portfolio management to solve problems and use cases associated with product managers, business managers,
senior leadership roles, and executive roles.
- [Portfolio management](https://gitlab.com/groups/gitlab-org/-/epics/48)
  - [(Complete) Epics and roadmap](https://gitlab.com/groups/gitlab-org/-/epics/50)
  - [Managing epics](https://gitlab.com/groups/gitlab-org/-/epics/49)
  - [Filtered roadmaps across time](https://gitlab.com/groups/gitlab-org/-/epics/51)
  - [Close epics](https://gitlab.com/groups/gitlab-org/-/epics/145)
  - [Capacity planning and epic tracking in roadmap](https://gitlab.com/groups/gitlab-org/-/epics/76)
  - [Manage issues within epic](https://gitlab.com/groups/gitlab-org/-/epics/4)
  - [Team dashboard](https://gitlab.com/groups/gitlab-org/-/epics/135)
  - [Group milestone as a release (in addition to a sprint)](https://gitlab.com/groups/gitlab-org/-/epics/69)
  - [Autocomplete in epics](https://gitlab.com/groups/gitlab-org/-/epics/48)
  - [Make epics easier to use](https://gitlab.com/groups/gitlab-org/-/epics/134)
  - [Validations for adding issues to and removing issues from epic](https://gitlab.com/groups/gitlab-org/-/epics/7)  
  - [Project-level epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/4019)
  - [Subepics](https://gitlab.com/gitlab-org/gitlab-ee/issues/4282)
  - [Risk management](https://gitlab.com/gitlab-org/gitlab-ee/issues/3978)
  - [What-if scenario planning](https://gitlab.com/gitlab-org/gitlab-ee/issues/3979)

- [Open issue boards issues](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=boards&scope=all&sort=milestone&state=opened)
- [Swimlanes in board](https://gitlab.com/gitlab-org/gitlab-ee/issues/979)
- [Images in boards](https://gitlab.com/gitlab-org/gitlab-ce/issues/33666)
- [Related merge requests in board cards](https://gitlab.com/gitlab-org/gitlab-ce/issues/25669)

#### Jira integration
Our users continue to rely on Jira. We are building deep connections with Jira into GitLab, so that Jira users can benefit from integrated experiences and workflows. We hope that our users can use GitLab for issue management and other portfolio management features. But we will nonetheless continue to support deep integrations with Jira as a priority to meet our users where they are.
- [Better than Atlassian Jira integration](https://gitlab.com/gitlab-org/gitlab-ce/issues/27073)
- [Jira development panel integration](https://gitlab.com/groups/gitlab-org/-/epics/73)
- [Import Jira issues to GitLab issues](https://gitlab.com/groups/gitlab-org/-/epics/10)

## Moonshots

Moonshots are big hairy audacious goals that may take a long time to deliver.

<%= wishlist["moonshots"] %>

## Scope

[Our vision](#vision) is to replace disparate DevOps toolchains with a single integrated application that is pre-configured to work by default across the complete DevOps lifecycle. Consider viewing [the presentation of our plan for 2018](https://about.gitlab.com/2017/10/11/from-dev-to-devops/).

Inside our scope are the 7 stages of the DevOps lifecycle as detailed on [our features page](https://about.gitlab.com/features/). Also see our [complete DevOps vision](https://about.gitlab.com/2017/10/11/from-dev-to-devops/) for where we're going in 2018.

We try to prevent maintaining functionality that is language or platform specific because they slow down our ability to get results. Examples of how we handle it instead are:

1. We don't make native mobile clients, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems, people can use [Tower](https://www.git-tower.com/mac/) and for example GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/ci/examples/sast.html#supported-languages-and-frameworks).
1. For code navigation we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) we reuse Codeclimate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) we use Heroku Buildpacks.

Outside our scope are:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

## ML/AI at GitLab

Machine learning (ML) through neural networks is a really great tool to solve hard to define, dynamic problems.
Right now, GitLab doesn't use any machine learning technologies, but we expect to use them in the near future
for several types of problems:

### Signal / noise separation

Signal detection is very hard in an noisy environment. GitLab plans to use
ML to warn users of any signals that stand out against the background noise in several features:

- security scans, notifying the user of stand-out warnings or changes
- error rates and log output, allowing you to rollback / automatically rollback a change if the network notices abberant behavior

### Recommendation engines

Automatically categorizing and labelling is risky. Modern models tend to overfit, e.g. resulting
in issues with too many labels. However, similar models can be used very well in combination
with human interaction in the form of recommendation engines.

- suggest labels to add to an issue / MR (one click to add)
- suggest a comment based on your behavior
- suggesting approvers for particular code

### Smart behavior

Because of their great ability to recognize patterns, neural networks are an excellent
tool to help with scaling, and anticipating needs. In GitLab, we can imagine:

- auto scaling applications / CI based on past load performance
- prioritizing parallized builds based on the content of a change

### Code quality

Similar to [DeepScan](https://deepscan.io/home/).

### Code navigation

Similar to [Sourcegraph](https://about.sourcegraph.com/).

## Large files

Git was primarily designed for code, where it is dominating the world.
Organisations working with large files, for instance art assets when creating
films or games, are looking to get the power that GitLab offers, but can't adopt because of
the little support for large files that Git offers.

GitLab is going to help these organisations adopt Git and migrate away from legacy platforms such
as Perforce. There are a number of moving parts that make this possible:

### Git LFS

Git LFS allows you to work well with large files in Git.

<%= wishlist["lfs"] %>

### File locking

File locking is crucial to collaborating on non-mergeable files.
Currently GitLab offers branch-limited file locking and Git LFS offers
locking across branches.

<%= wishlist["file locking"] %>

## Product Strategy

GitLab allows you to develop and operate  in GitLab, from planning to monitoring.

GitLab provides an [_single application_](https://about.gitlab.com/handbook/product/single-application/) that [_plays well with others_](#plays-well-with-others) for [_teams of any size_](#teams-of-any-size) with [_any kind of projects_](#any-project), while giving you [_actionable feedback_](#actionable-feedback).

## Plays well with others

We understand that not everyone will use GitLab for everything all the time, especially when first adopting GitLab.
We want you to use more of GitLab because you love that part of GitLab.
GitLab plays well with others, even when you use only one part of GitLab it should be a great experience.

GitLab ships with built-in integrations to many popular applications. We aspire to have the worlds best integrations for Slack, JIRA, and Jenkins.

Many other applications [integrate with GitLab](https://about.gitlab.com/integrations/), and we are open to adding new integrations to our [applications page](https://about.gitlab.com/applications/). New integrations with GitLab can very in richness and complexity; from a simple webhook, and all the way to a [Project Service](https://docs.gitlab.com/ee/user/project/integrations/project_services.html).

GitLab [welcomes and supports new integrations](https://about.gitlab.com/integrations/) to be created to extend collaborations with other products.
GitLab plays well with others by providing APIs for nearly anything you can do within GitLab.
GitLab can be a [provider of authentication](https://docs.gitlab.com/ee/integration/oauth_provider.html) for external applications.
And of course GitLab is open source so people are very welcome to add anything that they are missing.
If you are don't have time to contribute and am a customer we gladly work with you to design the API addition or integration you need.

## Actionable Feedback

Deployments should never be fire and forget. GitLab will give you immediate
feedback on every deployment on any scale. This means that GitLab can tell you
whether performance has improved on the application level, but also whether
business metrics have changed.

Concretely, we can split up monitoring and feedback efforts within GitLab in
three distinct areas: execution (cycle analytics), business and system feedback.

### Business feedback

With the power of monitoring and an integrated approach, we have the ability to
do amazing things within GitLab. GitLab will be able to automatically test
commits and versions through feature flags and A/B testing.

Business feedback exists on different levels:

* Short term: how does a certain change perform? Choose A/B based on data.
* Medium term: did a particular new feature change conversions, engagement
* Long term: how do larger efforts relate to changes in conversations, engagement, revenue

- [A/B Testing of branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)

### Application feedback

You application should perform well after changes are made. GitLab will be able to
see whether a change is causing errors or performance issues on application level.
Think about:

* Response times of e.g. a backend API
* Error rates and occurrences of new bugs
* Changes in API calls

### System feedback

We can now go beyond CI and CD. GitLab will able to tell you whether a change
improved performance or stability. Because it will have access to both
historical data on performance and code, it can show you the impact of any
particular change at any time.

System feedback happens over different time windows:

* Immediate: see whether changes influence availability and alert if they do
* Short-medium term: see whether changes influence system metrics and performance
* Medium-Long term: did a particular effort influence system status

- Implemented: [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html)
- [Status monitoring and feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/25555)
- [Feature monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/24254)

### Execution Feedback & Cycle Analytics

GitLab is able to speed up cycle time for any project.
To provide feedback on cycle time GitLab will continue to expand cycle
analytics so that it not only shows you what is slow, it’ll help you speed up
with concrete, clickable suggestions.

- [Cycle Speed Suggestions](https://gitlab.com/gitlab-org/gitlab-ce/issues/25281)

## Why cycle time is important

The ability to monitor, visualize and improve upon cycle time (or: time to value) is fundamental
to GitLab's product. A shorter cycle time will allow you to:

- respond to changing needs faster (i.e. skate to where the puck is going to be)
- ship smaller changes
- manage regressions, rollbacks, bugs better, because you're shipping smaller changes
- make more accurate predictions
- focus on improving customer experience, because you're able to respond to their needs faster

When we're adding new capabilities to GitLab, we tend to focus on things that
will reduce the cycle time for our customers. This is why we choose
[convention over configuration](/handbook/product/#convention-over-configuration)
and why we focus on automating the entire software development lifecycle.

All friction of setting up a new project and building the pipeline of tools
you need to ship any kind of software should disappear when using GitLab.

## Enterprise Editions

GitLab comes in 4 editions:
* **Libre**: This edition is aimed at solo developers or teams that
do not need advanced enterprise features. It contains a complete stack with all
the tools developers needs to ship software.
* **Starter**: This edition contains features that are more
relevant for organizations that have more than 100 potential users. For example:
	* features for managers (reports, management tools at the group level,...),
	* features targeted at developers that have to work in multi-disciplinary
	teams (merge request approvals,...),
	* integrations with external tools.
* **Premium**: This edition contains features that are more
relevant for organizations that have more than 750 potential users. For example:
	* features for instance administrators
	* features for managers at the instance level (reporting, management tools,
	roles,...)
	* features to help teams that are spread around the world
	* features for people other than developers that help ship software (support,
	QA, legal,...)
* **Ultimate**: This edition contains
features that are more relevant for organizations that have more than 5000
potential users. For example:
	* compliances and certifications,
	* change management.

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs) are publicly viewable.

## Vision

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, monitored, and documented. Stitching all these stages of the DevOps lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that a **single application for the DevOps lifecycle based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from planning to monitoring**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.
