---
layout: markdown_page
title: GitLab for Education
---

GitLab is being widely used by educational institutions around the world.

# Educational Pricing

GitLab’s Educational pricing policy applies to self-hosted GitLab only.
However, Starter, Premium and Ultimate are all applicable under the policy.

GitLab's Educational pricing is for educational institutions, As per this model only staff are counted towards the 'user count', Students can be added free of charge after a purchase for staff users.

To avail of this offer please contact our sales team at https://about.gitlab.com/sales/