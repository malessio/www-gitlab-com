---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Roles

- **PM** - Product Management or Product Manager. The [product team](https://about.gitlab.com/handbook/product/) as a whole, or the specific person responsible for a product area.
- **PMM** - Product Marketing Management or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.

## What is PMM working on?
- View the [Product Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/397296?=&label_name[]=Product%20Marketing) to see what's currently in progress.
- To ask for PMM resources log an issue in the marketing project and label it with `Product Marketing`.
- The PMM team does sprint planning on a biweekly basis where the backlog is reviewed and issues are prioritized.
- If you need more immediate attention please send a message in the `#product-marketing` slack channel.

## Release vs Launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

## Tiers

### Overview

| Tier      | Delivery                  | License                   | Fee            | Time      |
| --------- | ------------------------- | ------------------------- | -------------- | --------- |
| Libre     | Self-hosted               | Open Source               | Gratis         | Unlimited |
| Trial     | Self-hosted               | Proprietary               | Gratis         | Limited   |
| Starter   | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Premium   | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Ultimate  | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Free      | GitLab.com                | Open Source               | Gratis         | Unlimited |
| Pilot     | GitLab.com                | Proprietary               | Gratis         | Limited   |
| Bronze    | GitLab.com                | Proprietary               | Paid           | Unlimited |
| Silver    | GitLab.com                | Proprietary               | Paid           | Unlimited |
| Gold      | GitLab.com                | Proprietary               | Paid           | Unlimited |

### Definitions

1. Users: anyone who uses GitLab regardless of tier.
1. Customers: users on a paid tier.
1. Plans: the paid tiers only.
1. Time-limited tiers: pilots (GitLab.com) and trials (self-hosted).
1. Subscription: a combination of the paid tiers and the time-limited tiers, for example the trial tier is a subscription but not a plan.
1. License: open source vs. proprietary, for example moving a feature from a proprietary tier to the open-source tier.
1. Tier: a combination of the subscription tiers and the open source licensed tiers.
1. Distribution: self-hosted CE vs. EE, for example you can have a EE distribution but in the Libre tier.
1. Version: the [release of GitLab](https://about.gitlab.com/releases/), for example asking what version a user is on.

### Delivery

In general each of the five self-hosted tiers match the features in the GitLab.com tiers. They have different names for two reasons:

1. There is not complete feature parity between self-hosted and GitLab.com plans. For example, Starter, Premium, and Ultimate include [LDAP Group Sync](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html#group-sync) but Bronze, Silver and Gold do not.
1. We want to know if a user is using self-hosted or GitLab.com based on a just the tier name to prevent internal and external confusion.

When we need to say in one word tier a feature is in (for example on our issue tracker) we use the self-hosted tiers because they tend to contain a superset of the GitLab.com tier features.
Where we can we show both the self-hosted and the GitLab.com tiers, do example in [a release post](https://about.gitlab.com/2018/02/22/gitlab-10-5-released/#instant-ssl-with-lets-encrypt-for-gitlab).

### Libre, Gratis, and Free
Libre, Gratis, and Free are terms used in the open source community. "Free" is an ambiguous term that can means either free as in "no cost" (e.g. $0 "free as in beer"), free as in "with few or no restrictions" (e.g. "free as in free speech"), or both. "Gratis" is an unambiguous term to mean "no cost" while "Libre" is an unambiguous term to mean "with few no restrictions." Open source software is "libre" in that it is free to inspect, modify, and redistribute. Open source software may or may not be "gratis." Our time-limited tiers are gratis but not open source. Our Free and Libre tiers refer to our open source software that is both [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). For more info see the [wikipedia article](https://en.wikipedia.org/wiki/Gratis_versus_libre).

### Personal vs Group subscriptions
GitLab.com subscriptions are added to either a personal namespace or a group namespace. Personal subscriptions apply to a single user while Group subscriptions apply to all users in the Group.

### Distributions
Libre users can use either one of two distributions: Community Edition (CE) and Enterprise Edition (EE). Enterprise Edition can be downloaded, installed, and run without a commercial subscription. In this case it runs using the open source license and only has access to the open source features. In effect, EE without a subscription, and CE have the exact same functionality. The advantage of using EE is that it is much easier to upgrade to a commercial subscription later on. All that's needed is to install a license key to access more features vs needing to re-install a different distribution.

**Note**: The terms CE & EE refer solely to the software distribution, not to the subscription plan. **Never use CE vs. EE as a substitute for versions.** Instead, talk about open source users vs. proprietary.

### GitLab Trial
Today, we only offer a [free trial for self-hosted GitLab](https://about.gitlab.com/free-trial/). The trial offers all of the features of Gitlab Premium. In the furutre the trial will [offer all the features of Ultimate](https://gitlab.com/gitlab-com/marketing/issues/2099). Users on the Libre (self-hosted) and Free (GitLab.com) plans get access for an unlimited amount of time to a limited set of features. Trial users get access to a full set of features for a limited amount of time (30-days).

| License type | Features | Time Period |
| ------------ | -------- | ----------- |
| Libre & Free | Limited (Open source features only) | unlimited |
| Trial        | Unlimited (access to all Premium features) | limited (30 days) |

In the future we might introduce a similar concept for GitLab.com. We'll call this a pilot to make sure you can tell the delivery mechanism from the tier.

### Open source projects on GitLab.com get all Gold features.
The GitLab.com Free plan offers unlimited public and private repos and unlimited contributors but has limited features for private repos. Private repos only get access to the open source features. Public projects get access to all the features of Gold free of charge. This is to show our appreciation for Open Source projects hosted on GitLab.com.

### Messaging dos and don'ts

- Don't use the terms `Enterprise Edition Starter`, `Enterprise Edition Premium`, `Enterprise Edition Ultimate`, `EES`, `EEP`, or `EEU`. These have all been deprecated.
- Don't use `Community Edition`, `CE`, `Enterprise Edition`, or `EE` to refer to tiers.
- Don't use "edition" to refer to plans like `Starter Edition` or `Premium Edition` - Starter, Premium, and Ultimate are tiers, not "editions" of the software.
- Don't use "enterprise" as a modifier for tiers such as `Enterprise Starter`, `Enterprise Premium`, or `Enterprise Ultimate`.

- Do refer to plans by their stand-alone name: Libre, Starter, Premium, Ultimate, Free, Silver, Bronze, and Gold.
- Do optionally use "GitLab" as a modifier for plan names: GitLab Libre, GitLab Starter, GitLab Premium, GitLab Ultimate, GitLab Free, GitLab Silver, GitLab Bronze, and GitLab Gold.
- Do use "enterprise" to a market segment. e.g. good phrases to use are: "GitLab provides DevOps for the enterprise", "GitLab is enterprise-ready", "GitLab has many enterprise customers", and "GitLab provides enterprise software for the complete DevOps lifecycle."
- Do use `Community Edition`, `CE`, `Enterprise Edition` and `EE` to refer to our software distributions. Encourage customers to use the EE distribution since it provides the least painful upgrade path if/when users discover they need commercial features.
- Do use the terms "Open Source" and "Proprietary". Unlike other terms that are GitLab-insider language (e.g. "Free" vs "Bronze") almost everyone will immediately understand the difference. Calling our proprietary tiers closed source doesn't make sense because even our proprietary source code is publicly viewable.

## Elevator pitch

**The Problem - Customer Perspective**

Right now every large enterprise is suffering from a lack of consistency:

* Not using the same tools
* Not using the same integrations
* Not using the same configuration
* Not using the same work processes
* Not being judged on the same metrics

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production that needs to approve releases
* SOX compliance sign offs that need to happen
* QA cycles
* Separate QA teams
* Separate build teams

**Solution**

### Single Sentence

GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 3 times faster and radically improving the speed of business.

### Short Message
(~50 words)

GitLab is the first single application for all stages of the DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of the toolchain. GitLab provides unmatched visibility, higher levels of efficiency, and comprehensive governance. This make the software lifecycle 3 times faster, radically improving the speed of business.

### Medium Message
(~250 words)
GitLab is the first single application for all stages of the DevOps lifecycle. Only GitLab enables Concurrent DevOps, unlocking organizations from the constraints of today’s toolchain. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect. This make the software lifecycle 3 times faster, radically improving the speed of business.

GitLab and Concurrent DevOps collapses cycle times by driving higher efficiency across all stages of the software development lifecycle. For the first time, Product, Development, QA, Security, and Operations teams can work concurrently in a single application. There’s no need to integrate and synchronize tools, or waste time waiting for handoffs. Everyone contributes to a single conversation, instead of managing multiple threads across disparate tools. And only GitLab gives teams complete visibility across the lifecycle with a single, trusted source of data to simplify troubleshooting and drive accountability. All activity is governed by a consistent controls, making security and compliance first-class citizens instead of an afterthought. 

Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.  

### Long Message
(~450 Words)
GitLab is the first single application for software development, security, and operations that enables Concurrent DevOps, making the software lifecycle 3 times faster and radically improving the speed of business. Only GitLab provides a single application that unlocks organizations from the compromises and constraints of today’s DevOps Toolchain, significantly improving visibility, efficiency and governance. Now, fast paced teams no longer have to integrate or synchronize multiple DevOps tools and are able to go faster by working seamlessly across the complete lifecycle. 

GitLab delivers complete real-time visibility of all projects and relevant activities across the expanded DevOps lifecycle. For the first time, teams can see everything that matters. Changes, status, cycle times, security and operational health are instantly available from a trusted single source of data. Information is shown where it matters most, e.g. production impact is show together with the code changes that caused it. And developers see all relevant security and ops information for any change. With GitLab, there is never any need to wait on synchronizing your monitoring app to version control or copying information from tool to tool. GitLab frees teams to manage projects, not tools. These powerful capabilities eliminate guesswork, help teams drive accountability and gives everyone the data-driven confidence to act with new certainty.  With Gitlab, DevOps teams get better every day by having the visibility to see progress and operate with a deeper understanding of cycle times across projects and activities. 

GitLab drives radically faster cycle times by helping DevOps teams achieve higher levels of efficiency across all stages of the lifecycle. Concurrent DevOps makes it possible for Product, Development, QA, Security, and Operations teams to work at the same time, instead of waiting for handoffs. Teams can work concurrently and review changes together before pushing to production. And everyone can contribute to a single conversation across every stage. Only GitLab eliminates the need to manually configure and integrate multiple tools for each project. Teams can start immediately and work concurrently to radically compress time across every stage of the DevOps lifecycle.    

Only GitLab delivers DevOps teams powerful new governance capabilities embedded across the expanded lifecycle to automate security, code quality and vulnerability management. With GitLab, tighter governance and control never slow down DevOps speed. 

GitLab leads the next advancement of DevOps. Built on Open Source, GitLab  delivers new innovations and features on the same day of every month by leveraging contributions from a passionate, global community of thousands of developers and millions of users. Over 100,000 of the world’s most demanding organizations trust GitLab to realize the transformative power of Concurrent DevOps to achieve a 3x faster lifecycle.

### Is it similar to GitHub?

GitLab started as an open source alternative to GitHub. Instead of focusing on hosting open source projects we focused on the needs of the enterprise, for example we have 5 authorization levels vs. 2 in GitHub. Now we've expanded the feature set with continuous integration, continuous delivery, and monitoring.

### What is the benefit?

We help organizations go faster to market by reducing the cycle time and simplifying the toolchain development and operations use to push their software to market. Before GitLab you needed 7 tools, it took a lot of effort to [integrate](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) them, and you end up with have different setups for different teams. With GitLab you gain visibility on how long each part of the software development lifecycle is taking and how you can improve it.

### Email Intro

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an [integrated product](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) for the entire software development lifecycle. It contains not only issue management, version control and code review but also Continuous Integration, Continuous Delivery, and monitoring. Organizations all around the world, big and small, are using GitLab. In fact, 2/3 of organizations that self-host git use GitLab. That is more than 100,000 organizations and millions of users.

### [GitLab Positioning](/handbook/positioning-faq/)

### [GitLab Demo](/handbook/marketing/product-marketing/demo/)

## Customer references repository

Today, we don't have a central repository for customer service, but this is a Q1 goal to start building one. For now we are capturing customer stories in an issue. If you have a customer story or anecdote [leave a comment on issue 1834](https://gitlab.com/gitlab-com/marketing/issues/1834).

To initiate a formal case study process, follow the process listed [here](/marketing-sales-development/content/#customer-case-study-process)

## Print Collatoral

- [GitLab DataSheet](https://about.gitlab.com/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)

## Pitch Deck

### Prod
[Production Deck](https://docs.google.com/presentation/d/1LPGgRLf11FQ_a1dv72oOOzb6l9GnJnZ_LCCu8EijP8A)
There is one canonical slide deck, known as "The Production Deck" that contains the GitLab narrative and pitch. The prod deck should always be in a state that is ready to present. This is a deck everyone should present from and/or fork from when there is a need to create new content tailored to a specific audience (analysts, events, specific customers, etc.). As such there should never be comments or WIP slides in the Prod deck. Only the pitch deck production engineer (William Chia) has write access to the production environment. Changes, comments, and additions should be made in the staging deck.

### Staging
[Staging Deck](https://docs.google.com/presentation/d/11eotTy_A4ZG444mgaCQbd-eglWBeLOMkvcO9cC7LWQQ/edit#slide=id.g3092f78080_20_4)

To update the Staging deck, create a copy of the slide you want to modify, or add the new slide you want to suggest. Create an issue in the Marketing Issue tracker and label with 'Product Marketing' including a link to your slide and the title. The changes can the be discussed and revised until the slide is ready to be deployed to production.

### QA
Slides should go through QA first. This includes
- Spell and grammar check
- Technical accuracy check
- Links check (do links go to the right place)
- Brand / design review
- Slide notes are complete

## How to deploy a staging slide to production
When a slide has passed QA the slide deck production engineer should copy and paste it into the production deck. The change should be communicated in the #sales slack channel. Additional enablement should be added to the weekly sales call if necessary.


## Competitive Intelligence

- Post any competitive info in the `#competition` slack channel.
- Link to the blog post, announcement, or feature page you are referencing.
- If you received the intel from a customer conversation, link to the Salesforce record and the full set of notes from the conversation.

## Press release boiler plate
GitLab is the first single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project.  GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time and focus exclusively on building great software quickly.  Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel trust GitLab to deliver great software at new speeds.

## Analyst product categorizations

GitLab is a single application that spans many product categories.
Forrester and Gartner define several of these categories and their definition
of a space can be useful to determine what important competing products are.
Below, some relevant categories are listed that GitLab is or will be competing in.

### Forrester: Continuous Integration Tools

- Leaders: GitLab CI, CloudBees Jenkins, CircleCI, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Integration+Tools+Q3+2017/-/E-RES137261)

### Forrester: Configuration Management Software For Infrastructure Automation

- Leaders: Puppet Enterprise, Chef Automate
- [Link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)

### Gartner: Software Change and Configuration Management Software

- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [Link to report](https://www.gartner.com/document/3118917)
- Market Guide update initiated March 2018.

### Gartner: Continuous Configuration Automation Tools

- Market Guide: Chef, CFEngine, Inedo, Orca, Puppet, Red Hat, SaltStack
- [Link to report](https://www.gartner.com/document/3843365)

### Gartner: Software Test Automation

- Leaders: MicroFocus, Tricentis
- [Link to 15 November 2016 report](https://www.gartner.com/document/3512920)
- [Link to 20 November 2017 report](https://www.gartner.com/document/3830082)

### Forrester: Modern Application Functional Test Automation Tools

- Leaders: Parasoft, IBM, Tricentis, HPE
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)

### Gartner: Performance Testing

- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [Link to report](https://www.gartner.com/document/3133717)

### Gartner: Application Release Automation

- Leaders: Electric Cloud, CA (Automic), XebiaLabs, IBM
- [Magic quadrant](http://electric-cloud.com/resources/whitepapers/gartner-magic-quadrant-application-release-automation/)
- MQ update underway. Research kicked off March 2018


### Forrester: Continuous Delivery and Release Automation

- Leaders: Chef, CA, Microsoft
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q3+2017/-/E-RES137969)

### Gartner: Application Performance Monitoring Suites

- Leaders: New Relic
- [Link to report](https://www.gartner.com/doc/3551918)

### Gartner: Application Security Testing

- Leaders: HPE, Veracode, IBM, Synopsys, WhiteHat Security
- [Link to report](https://www.gartner.com/doc/3623017)

### Forrester: Application Security Testing

- Leaders: Synopsys, CA Veracode
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)

### Gartner: Project Portfolio Management

- Leaders: Planview, CA Technologies, Changepoint
- [Link to report](https://www.gartner.com/document/3728917)

### Forrester: Strategic Portfolio Management Tools

- Leaders: AgileCraft, CA, ServiceNow
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)

### Forrester: Portfolio Management For The Tech Management Agenda

- Leaders: CA, Planview
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Portfolio+Management+For+The+Tech+Management+Agenda+Q1+2015/-/E-RES114742)

### Gartner: Enterprise Agile Planning Tools

- Leaders: CA, Atlassian, VersionOne
- [Link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

### Forrester: Enterprise Collaborative Work Management

- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)

### Forrester: Application Life-Cycle Management, Q4 2012

- Covered in report: Atlassian, CollabNet, HP, IBM, Microsoft, PTC, Rally Software, Rocket Aldon, and Serena Software
- [Link to report](https://www.forrester.com/report/The+Forrester+Wave+Application+LifeCycle+Management+Q4+2012/-/E-RES60080)

### Gartner: Container Management Software

- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [Link to report](https://www.gartner.com/document/3782167)

### Gartner: Enterprise Application Platform as a Service

- Leaders: Salesforce, Microsoft
- [Link to report](https://www.gartner.com/document/3263917)

### Gartner: Data Science Platforms

- Leaders: IBM, SAS, RapidMiner, KNIME
- [Link to report](https://www.gartner.com/doc/3606026)

## Partner Marketing

### Partner Marketing Objectives

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

### Partner Marketing Activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

### Partner Newsletter Feature

In line with the objective of "Promote existing partnerships to be at top-of-mind for developers", a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

## Channel Marketing

### Channel Marketing Objectives

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote [Reseller program](https://about.gitlab.com/resellers/program/) to recruit new resellers

### Reseller Funds Allocation Determination

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the Google Drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLab, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.
