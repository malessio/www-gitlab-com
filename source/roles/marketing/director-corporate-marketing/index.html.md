---
layout: job_page
title: "Director, Corporate Marketing"
---

As the Director, Corporate Marketing, you will lead the [Corporate Marketing team](https://about.gitlab.com/handbook/marketing/#corporate-marketing) responsible for building the GitLab brand and sharing the GitLab story globally. This role will successfully promote the company and its customers through multi-channel global programs and corporate events, development and execution of an authentic and engaging content marketing strategy, and delivering a bold marketing website strategy. The qualified candidate will be a driven and engaged senior leader with a successful track record of building and leading, brand, global programs and content strategy globally. Reporting to the CMO, the Director, Corporate Marketing must have experience across content marketing, public relations, brand development, creative, multimedia, digital marketing, social media, customer marketing, and thought leadership campaigns.

## Responsibilities

* Work closely with the Marketing leadership team to contribute to the definition of the strategic objectives for marketing and build strong working relationships to deliver as a team.
* Define and execute corporate marketing and brand strategy, resulting in consistent branding, messaging and positioning.
* Create and execute a customer marketing strategy that builds on the success of GitLab customers.
* Drive a superior content marketing program that drives awareness of GitLab, the related business challenges and GitLab’s unique solution.
* Create, architect and promote corporate content for all stages of the buyer journey.
* Operate a digital and social media marketing plan that expands the reach and recognition of GitLab and educates the market on GitLab’s unique market position.
* Oversee global event strategy and execution for conferences and industry events, and support internal events.
* Manage corporate communications including executive communications, messaging development, and PR
* Define and deploy best practice processes for supporting broad array of creative requirements from the marketing team.
* Support functional groups with marketing services such as creative, writing, event planning, and video.
* Manage, build, and lead a strong team by recruiting, coaching and developing team members.

## Requirements

* 10+ years of experience in corporate marketing in the software industry, preferably within an area of application development.
* Technical background or good understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Proven track record developing and executing successful B2B corporate communications, content and brand campaigns.
* Skilled and proven experience bringing new campaigns and ideas to life.
* Experience leading the development of exciting and effective awareness and brand marketing campaigns that influence IT leadership and CxO buyers.
* Track record of evolving a brand’s position to a new market category that aligns it with current market and customer priorities.
* History successfully utilizing events as a key platform for growth in the awareness and demand generation.
* Proven track record in building, getting buy-in and executing marketing plans, and staying focused on “getting it done” in a fast-moving, technical environment.
* Able to coordinate across many teams and perform in fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Uses data to measure results and inform decision making and strategy development.
* You share our [values](/handbook/values), and work in accordance with those values.

### Draft OKRs

* Develop integrated campaigns (target definition, content, assets, etc) such as: CE to EE conversion, frustrations with legacy solutions, continuous development, cloud native. Also customer education/adoption focused campaign. Support region field marketing in campaign execution. Verticalize campaigns to target priority verticals including Financial Services. Metrics to include # of campaigns, and inquiry and SCLAU goals.
* Messaging: finalize new positioning/messaging, create umbrella campaign and activate messaging through earned (PR) and owned (about.gitlab.com and social) channels to position GitLab as market leader.
* Marketing website: activate new messaging on website, finalize information architecture plan and implement iterative site enhancements. Partner with Online Growth team on trials user experience and conversion rate optimization. Metrics: grow visitors, increase time on sight and page views, increase trial conversion rate and total trial downloads month over month.
* PR - thought leadership platform on Forbes, Quartz; grow share of voice (SOV) through feature placement and announcement coverage. Metrics: # articles; SOV % and growth
* Q1 corporate events - IBM Think. Metrics: lead goal; # at event meetings; speaking session(s); SCLAU goal.

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Chief Marketing Officer
* A 45 minute interview with our Senior Director of Marketing and Sales Development
* A 45 minute interview with our Vice President of Product
* A 45 minute interview with our Chief Revenue Officer
* Finally, our CEO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring).
